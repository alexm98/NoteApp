﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace noteapp
{
    public partial class settingswindow : Form
    {
        public settingswindow()
        {
            InitializeComponent();
        }

        private void settingswindow_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 0;
            comboBox3.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string fontsize = comboBox3.Text;
            string fontcolor = comboBox2.Text;
            string backgroundcolor = comboBox1.Text;

            XmlDocument xmlconfig = new XmlDocument();
            xmlconfig.LoadXml("<configsettings></configsettings>");

            XmlElement fontsizeconfig = xmlconfig.CreateElement("fontsize");
            XmlElement fontcolorconfig = xmlconfig.CreateElement("fontcolor");
            XmlElement backgroundconfig = xmlconfig.CreateElement("background");

            fontsizeconfig.InnerText = fontsize;
            fontcolorconfig.InnerText = fontcolor;
            backgroundconfig.InnerText = backgroundcolor;
          
            xmlconfig.DocumentElement.AppendChild(fontsizeconfig);
            xmlconfig.DocumentElement.AppendChild(fontcolorconfig);
            xmlconfig.DocumentElement.AppendChild(backgroundconfig);

            xmlconfig.Save("config.xml");
        }
         
    }
}
