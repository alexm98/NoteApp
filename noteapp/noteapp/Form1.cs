﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace noteapp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int index = 0; 

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Enabled = true)
            {
                var confirmResult = MessageBox.Show("Once activated, HTML is undoable on your file.","Confirm HTML File",
                                    MessageBoxButtons.OKCancel);
                if (confirmResult == DialogResult.OK)
                {
                    richTextBox1.Text = richTextBox1.Text + "<!DOCTYPE html>" + Environment.NewLine + "<html>" + Environment.NewLine + "    <head>" + Environment.NewLine + "        <title>Page Title</title>" + Environment.NewLine + "    </head>" + Environment.NewLine + "    <body>" + Environment.NewLine + " " + Environment.NewLine + "    </body>" + Environment.NewLine + "</html>";
                    checkBox1.Enabled = false;
                }
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //schimbarea fontului
            if (comboBox1.SelectedIndex == 0)
            {
                richTextBox1.SelectionFont = new Font("Calibri",8,FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 1)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 9, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 2)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 10, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 3)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 11, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 4)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 12, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 5)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 13, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 6)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 14, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 7)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 15, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 8)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 16, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 9)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 17, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 10)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 18, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 11)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 19, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 12)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 20, FontStyle.Regular);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            richTextBox1.SelectionFont = new Font("Calibri", 10, FontStyle.Regular);
            label4.Visible = false;
            comboBox1.SelectedIndex = 3;
        }

        private void Form1_MaximizedBoundsChanged(object sender, EventArgs e)
        {
            //richTextBox1.Width == this.Width;
        }
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Text Files (.txt)|*.txt|HTML Files(*.html)|*.html|All Files (*.*)|*.*";
            openFileDialog1.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            DialogResult result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                richTextBox1.LoadFile(openFileDialog1.FileName, RichTextBoxStreamType.PlainText);
                label4.Text = openFileDialog1.FileName;
                label4.Visible = true;
            }
        }
        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Text Files (.txt)|*.txt|HTML Files(*.html)|*.html|All Files (*.*)|*.*";
            saveFileDialog1.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            DialogResult saveresult = saveFileDialog1.ShowDialog();
            
            if (saveresult == DialogResult.OK)
            {
                richTextBox1.SaveFile(saveFileDialog1.FileName, RichTextBoxStreamType.PlainText);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string findword = textBox1.Text.ToString();
            richTextBox1.Find(findword);
            
        }

        private void richTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            var textindex = richTextBox1.SelectionStart;

            if(e.KeyChar == '{')
            {
                richTextBox1.Text = richTextBox1.Text.Insert(textindex, "}");
                richTextBox1.SelectionStart = textindex;
            }
            if(e.KeyChar == '(')
            {
                richTextBox1.Text = richTextBox1.Text.Insert(textindex, ")");
                richTextBox1.SelectionStart = textindex;
            }
            if(e.KeyChar == '"')
            {
                richTextBox1.Text = richTextBox1.Text.Insert(textindex, @"""");
                richTextBox1.SelectionStart = textindex;
            }
        }

        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.S && (ModifierKeys & Keys.Control) == Keys.Control)
            {
                pictureBox1_Click_1(pictureBox1, EventArgs.Empty);
            }
            if(e.KeyCode == Keys.O && (ModifierKeys & Keys.Control) == Keys.Control)
            {
                pictureBox2_Click(pictureBox2, EventArgs.Empty);
            }
        }
    }
}