﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace noteapp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int savedfile = 0;

        //html 
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Enabled == true)
            {
                var confirmResult = MessageBox.Show("Once activated, HTML is undoable on your file.", "Confirm HTML File",
                                    MessageBoxButtons.OKCancel);
                if (confirmResult == DialogResult.OK)
                {
                    richTextBox1.Text = richTextBox1.Text + "<!DOCTYPE html>" + Environment.NewLine + "<html>" + Environment.NewLine + "    <head>" + Environment.NewLine + "        <title>Page Title</title>" + Environment.NewLine + "    </head>" + Environment.NewLine + "    <body>" + Environment.NewLine + " " + Environment.NewLine + "    </body>" + Environment.NewLine + "</html>";
                    checkBox1.Enabled = false;
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //schimbarea fontului din checkbox
            if (comboBox1.SelectedIndex == 0)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 8, FontStyle.Regular);
                richTextBox1.Font = new Font("Calibri", 8, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 1)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 9, FontStyle.Regular);
                richTextBox1.Font = new Font("Calibri", 9, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 2)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 10, FontStyle.Regular);
                richTextBox1.Font = new Font("Calibri", 10, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 3)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 11, FontStyle.Regular);
                richTextBox1.Font = new Font("Calibri", 11, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 4)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 12, FontStyle.Regular);
                richTextBox1.Font = new Font("Calibri", 12, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 5)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 13, FontStyle.Regular);
                richTextBox1.Font = new Font("Calibri", 13, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 6)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 14, FontStyle.Regular);
                richTextBox1.Font = new Font("Calibri", 14, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 7)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 15, FontStyle.Regular);
                richTextBox1.Font = new Font("Calibri", 15, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 8)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 16, FontStyle.Regular);
                richTextBox1.Font = new Font("Calibri", 16, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 9)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 17, FontStyle.Regular);
                richTextBox1.Font = new Font("Calibri", 17, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 10)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 18, FontStyle.Regular);
                richTextBox1.Font = new Font("Calibri", 18, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 11)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 19, FontStyle.Regular);
                richTextBox1.Font = new Font("Calibri", 19, FontStyle.Regular);
            }
            if (comboBox1.SelectedIndex == 12)
            {
                richTextBox1.SelectionFont = new Font("Calibri", 20, FontStyle.Regular);
                richTextBox1.Font = new Font("Calibri", 20, FontStyle.Regular);
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            // initializarea programului cu setari default
            richTextBox1.SelectionFont = new Font("Calibri", 10, FontStyle.Regular);
            label4.Visible = false;
            comboBox1.SelectedIndex = 3;

            // citirea si initierea configuratiei din XML
            XmlDocument xmlconfig = new XmlDocument();
            xmlconfig.Load("config.xml");

            XmlNode xmlfontsize = xmlconfig.DocumentElement.SelectSingleNode("/configsettings/fontsize");
            string xmlfonts = xmlfontsize.InnerText;
            XmlNode xmlfontcolor = xmlconfig.DocumentElement.SelectSingleNode("/configsettings/fontcolor");
            string xmlfontc = xmlfontcolor.InnerText;
            XmlNode xmlbackground = xmlconfig.DocumentElement.SelectSingleNode("/configsettings/background");
            string xmlbg = xmlbackground.InnerText;

            // font color 
            if (xmlfontc == "Default")
            {
                richTextBox1.ForeColor = Color.Black;
            }
            else if (xmlfontc == "Black")
            {
                richTextBox1.ForeColor = Color.Black;
            }
            else if (xmlfontc == "White")
            {
                richTextBox1.ForeColor = Color.White;
            }
            else if (xmlfontc == "Blue")
            {
                richTextBox1.ForeColor = Color.Blue;
            }
            else if (xmlfontc == "Red")
            {
                richTextBox1.ForeColor = Color.Red;
            }
            else if (xmlfontc == "Yellow")
            {
                richTextBox1.ForeColor = Color.Yellow;
            }
            else if (xmlfontc == "Green")
            {
                richTextBox1.ForeColor = Color.Green;
            }

            //font size
            if (xmlfonts != "Default")
            {
                richTextBox1.SelectionFont = new Font("Calibri", Convert.ToInt32(xmlfonts), FontStyle.Regular);
            }

            // background
            if(xmlbg == "Default"){
                this.BackColor = System.Drawing.Color.LightSteelBlue;
                label1.ForeColor = Color.Black;
                label3.ForeColor = Color.Black;
                label4.ForeColor = Color.Black;
                label5.ForeColor = Color.Black;
                checkBox1.ForeColor = Color.Black;
            }
            else if (xmlbg == "Black")
            {
                this.BackColor = System.Drawing.Color.Black;
                label1.ForeColor = Color.White;
                label3.ForeColor = Color.White;
                label4.ForeColor = Color.White;
                label5.ForeColor = Color.White;
                checkBox1.ForeColor = Color.White;
            }
            else if (xmlbg == "White")
            {
                this.BackColor = System.Drawing.Color.White;
            }
            else if (xmlbg == "Blue")
            {
                this.BackColor = System.Drawing.Color.Blue;
            }
            else if (xmlbg == "Red")
            {
                this.BackColor = System.Drawing.Color.Red;
            }
            else if (xmlbg == "Yellow")
            {
                this.BackColor = System.Drawing.Color.Yellow;
            }
            else if (xmlbg == "Green")
            {
                this.BackColor = System.Drawing.Color.Green;
            }
        }

        private void Form1_MaximizedBoundsChanged(object sender, EventArgs e)
        {
            //richTextBox1.Width == this.Width;
        }

        //open
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Text Files (.txt)|*.txt|HTML Files(*.html)|*.html|All Files (*.*)|*.*";
            openFileDialog1.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            DialogResult result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                richTextBox1.LoadFile(openFileDialog1.FileName, RichTextBoxStreamType.PlainText);
                label4.Text = saveFileDialog1.FileName;
                label4.Text = openFileDialog1.FileName;
                label4.Visible = true;
            }
        }

        // save
        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                saveFileDialog1.Filter = "HTML Files(*.html)|*.html|All Files (*.*)|*.*";
            }
            else
            {
                saveFileDialog1.Filter = "Text Files (.txt)|*.txt|HTML Files(*.html)|*.html|All Files (*.*)|*.*";
            }
            saveFileDialog1.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

            DialogResult saveresult = saveFileDialog1.ShowDialog();

            if (saveresult == DialogResult.OK)
            {
                richTextBox1.SaveFile(saveFileDialog1.FileName, RichTextBoxStreamType.PlainText);
                savedfile = 1;
            }
        }

        // cautare in text
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (richTextBox1.Text != string.Empty)
                {
                    int index = 0;
                    String temp = richTextBox1.Text;
                    richTextBox1.Text = "";
                    richTextBox1.Text = temp;
                    while (index < richTextBox1.Text.LastIndexOf(textBox1.Text))
                    {
                        richTextBox1.Find(textBox1.Text, index, richTextBox1.TextLength, RichTextBoxFinds.None);
                        richTextBox1.SelectionBackColor = Color.Yellow;
                        index = richTextBox1.Text.IndexOf(textBox1.Text, index) + 1;
                        //richTextBox1.Select();
                    }
                }
            }
            catch (Exception mesaj)
            {
                MessageBox.Show(mesaj.Message, "There was an error while finding the word");
            }
        }

        // enter & search
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1.PerformClick();
            }
        }

        //autocomplete pentru paranteze,ghilimele si acolade
        private void richTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            var textindex = richTextBox1.SelectionStart;

            if (e.KeyChar == '{')
            {
                richTextBox1.Text = richTextBox1.Text.Insert(textindex, "}");
                richTextBox1.SelectionStart = textindex;
            }
            if (e.KeyChar == '(')
            {
                richTextBox1.Text = richTextBox1.Text.Insert(textindex, ")");
                richTextBox1.SelectionStart = textindex;
            }
            if (e.KeyChar == '[')
            {
                richTextBox1.Text = richTextBox1.Text.Insert(textindex, "]");
                richTextBox1.SelectionStart = textindex;
            }
            if (e.KeyChar == '"')
            {
                richTextBox1.Text = richTextBox1.Text.Insert(textindex, @"""");
                richTextBox1.SelectionStart = textindex;
            }
        }

        // shortcut-uri
        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S && (ModifierKeys & Keys.Control) == Keys.Control)
            {
                pictureBox1_Click_1(pictureBox1, EventArgs.Empty);
            }
            if (e.KeyCode == Keys.O && (ModifierKeys & Keys.Control) == Keys.Control)
            {
                pictureBox2_Click(pictureBox2, EventArgs.Empty);
            }
            if (e.KeyCode == Keys.R && (ModifierKeys & Keys.Control) == Keys.Control)
            {
                button2_Click_1(button2, EventArgs.Empty);
            }
            if (e.KeyCode == Keys.F && (ModifierKeys & Keys.Control) == Keys.Control)
            {
                textBox1.Focus();
            }
        }

        // verificare la inchidere pentru salvare
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (savedfile == 0)
            {
                var result = MessageBox.Show(this, "Are you sure you want to quit? You will loose the text in this instance.", "Warning",
                MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                if (result != DialogResult.Yes)
                {
                    e.Cancel = true;
                    return;
                }
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            // config menu
            settingswindow settings = new settingswindow();
            settings.Show();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            // replace
            Form2 replace = new Form2();
            replace.Show();
        }
}    
}