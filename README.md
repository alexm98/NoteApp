# NoteApp
Noteapp is a simple text editor wrote using C#

## What can it do?
1. It can search and replace words.
2. It can highlight words you search for.
3. It has a customizable user interface through a config XML file.
4. It has open/save options reminding you if you have unsaved changes.

### Here are some screenshots:
![alt text](https://raw.githubusercontent.com/alexm98/NoteApp/master/screenshots/print1.png)
![alt text](https://raw.githubusercontent.com/alexm98/NoteApp/master/screenshots/print9.png)
![alt text](https://github.com/alexm98/NoteApp/blob/master/screenshots/print8.png)

Feel free to use, modify, redistribute, modify and redistribute the code as it is licensed under GPLv3.
